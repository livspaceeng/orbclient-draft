from setuptools import find_packages, setup
setup(
    name="orbclientdraft",
    packages=find_packages(),
    version="0.1.2",
    description="Livspace Orb Client Draft library",
    author="vivek.paidi@livspace.com",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "urllib3 >= 1.15", "six >= 1.10", "certifi", "python-dateutil"
    ],
    dependency_links=[]
)
